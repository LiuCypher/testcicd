package com.bytel.ci

class Main {
    static void main(String[] args) {
        BinaryTree root = new BinaryTree("root")
        root.rightNode = new BinaryTree("rightNode")
        root.leftNode = new BinaryTree("leftNode")
        println root.getInPreOrder()
        println root.getInInnerOrder()
        println root.getInPostOrder()
    }
}
