package com.bytel.ci

class BinaryTree {
    String nodeValue
    BinaryTree rightNode
    BinaryTree leftNode

    BinaryTree(String nodeValue) {
        this.nodeValue = nodeValue
        this.rightNode = null
        this.leftNode = null
    }

    String getInPreOrder() {
        String res = ""
        res += this.nodeValue
        if (this.leftNode)
            res += " - ${this.leftNode.getInPreOrder()}"
        else
            res += " - null"
        if (this.rightNode)
            res += " - ${this.rightNode.getInPreOrder()}"
        else
            res += " - null"
        return res
    }

    String getInPostOrder() {
        String res = ""
        if (this.leftNode)
            res += "${this.leftNode.getInPostOrder()}"
        else
            res += "null"
        if (this.rightNode)
            res += " - ${this.rightNode.getInPostOrder()}"
        else
            res += " - null"
        res += " - ${this.nodeValue}"
        return res
    }

    String getInInnerOrder() {
        String res = ""
        if (this.leftNode)
            res += "${this.leftNode.getInInnerOrder()}"
        else
            res += "null"
        res += " - ${this.nodeValue}"
        if (this.rightNode)
            res += " - ${this.rightNode.getInInnerOrder()}"
        else
            res += " - null"
        return res
    }
}
