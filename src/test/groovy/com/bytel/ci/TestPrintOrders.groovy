package com.bytel.ci

import org.junit.jupiter.api.Test

class TestPrintOrders {

    @Test
    void testPreOrderString() {
        BinaryTree root = new BinaryTree("root")
        root.rightNode = new BinaryTree("rightNode")
        root.leftNode = new BinaryTree("leftNode")
        assert root.getInPreOrder() == "root - leftNode - null - null - rightNode - null - null"
    }

    @Test
    void testInnerOrderString() {
        BinaryTree root = new BinaryTree("root")
        root.rightNode = new BinaryTree("rightNode")
        root.leftNode = new BinaryTree("leftNode")
        assert root.getInInnerOrder() == "null - leftNode - null - root - null - rightNode - null"
    }

    @Test
    void testPostOrderString() {
        BinaryTree root = new BinaryTree("root")
        root.rightNode = new BinaryTree("rightNode")
        root.leftNode = new BinaryTree("leftNode")
        assert root.getInPostOrder() == "null - null - leftNode - null - null - rightNode - root"
    }

}
